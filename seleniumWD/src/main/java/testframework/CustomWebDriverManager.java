package testframework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.FirefoxDriverManager;

import org.openqa.selenium.remote.SessionId;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static io.github.bonigarcia.wdm.DriverManagerType.FIREFOX;


public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		private WebDriver driver = setupBrowser();


		private  WebDriver setupBrowser(){
			return  setupBrowserChrome();
//			return setupBrowserFirefox();
		}
		private WebDriver setupBrowserFirefox(){
			WebDriverManager.getInstance(FIREFOX).setup();
			WebDriver firefoxDriver = new FirefoxDriver();
			firefoxDriver.manage().window().maximize();
			return firefoxDriver;
		}
		private WebDriver setupBrowserChrome(){
			WebDriverManager.getInstance(CHROME).setup();
			WebDriver chromeDriver = new ChromeDriver();
			chromeDriver.manage().window().maximize();
			return chromeDriver;
		}
		public void quitDriver() {
			if (driver != null) {
				driver.quit();
				driver = null;
			}
		}
		public WebDriver getDriver() {
			if (driver == null) {
				driver = setupBrowser();
			}
			return driver;
		}
	}
}

//		private WebDriver setupBrowser(){
//			FirefoxDriverManager.getInstance().setup();
//			WebDriver firefoxDriver = new FirefoxDriver();
//			firefoxDriver.manage().window().maximize();
//			return firefoxDriver;
//		}
//
//		public void quitDriver() {
//			if (driver != null) {
//				driver.quit();
//			}
//		}
//
////		public WebDriver getDriver() {
////			return driver;
////		}
//public WebDriver getDriver() {
//	SessionId session;
////	if (browser.equalsIgnoreCase("FIREFOX")) {
////		session = ((FirefoxDriver) driver).getSessionId();
////	} else {
////		session = ((ChromeDriver) driver).getSessionId();
////	}
//	session = ((FirefoxDriver) driver).getSessionId();
//	if (session == null) {
//		driver = setupBrowser();
//	}
//	return driver;
//}
//
//	}
//}
