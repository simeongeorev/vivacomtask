package testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import java.util.List;

public class UserActions {
	final WebDriver driver;

	public UserActions() {
		this.driver = Utils.getWebDriver();
	}

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

    //############# CLICKERS ###############

	public void clickElementByXpath(String key){
		WebElement element =driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
		Utils.LOG.info("Clicking on element " + key);
	}

	public void clickElementJS (String locator){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		Utils.LOG.info("Clicking on element " + locator);
	}


	//############# WAITS ##############

	public void waitForElementClickable (String locator, int seconds){
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
		Utils.LOG.info("Waiting for "+locator+" element to be clickable");
	}

	public void fixedWait (int milliseconds){
		try {
			Thread.sleep(milliseconds);} catch (Exception next) {
		}
		Utils.LOG.info("Waiting fixed at "+ milliseconds +" milliseconds");
	}


	//############# ASSERTS ##############

	public void assertElementPresent(String locator){
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
		Utils.LOG.info("Assert: Element is present: " + locator);
	}

	public void assertCheckBoxIsChecked(String locator){
		Boolean isChecked = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).isSelected();
		Assert.assertEquals(true, isChecked);
		Utils.LOG.info("Assert: "+locator + " is checked");
	}

	public void assertElementNotPresent(String locator){
		Assert.assertTrue(driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).isEmpty());
		Utils.LOG.info("Assert: "+locator + " is not present");
	}
	//################### CHECKERS ######################

	public boolean checkIfElementNotPresent(String locator){
		Utils.LOG.info(locator + " is not present");
		return driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).isEmpty();
	}


	//################## SCROLLERS ######################

	public void scrollByVisibleElementJS(String locator) {
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("arguments[0].scrollIntoView()", element);
	}
	//################### BROWSER FUNCTIONS #####################

	public void refreshPage(){
		driver.navigate().refresh();
		Utils.LOG.info("Page refresh");
	}


    //################### GETTERS #######################

	public String getElementStringValue (String locator) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        Utils.LOG.info("String value: " + element.getText());
        return element.getText();
    }




}
