package vivacomPages;

public class DevicePage extends BasePage {


    public void addPhoneToCart () {
        acts.waitForElementClickable("smartL.CheckBox",SHORT_WAIT);
        acts.clickElementByXpath("smartL.CheckBox");
        acts.clickElementByXpath("acceptCookies.Btn");
        acts.waitForElementClickable("shoppingCart.Btn", SHORT_WAIT);
        acts.clickElementByXpath("shoppingCart.Btn");
    }
    public void addHeadphonesToCart () {
        acts.waitForElementClickable("shoppingCart.Btn", SHORT_WAIT);
        acts.clickElementByXpath("shoppingCart.Btn");
    }

}
