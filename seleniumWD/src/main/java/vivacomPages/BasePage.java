package vivacomPages;

import org.openqa.selenium.WebDriver;
import testframework.UserActions;
import testframework.Utils;

public class BasePage {

    protected static final int SHORT_WAIT = 5;
    protected static final int MEDIUM_WAIT = 10;
    protected static final int LONG_WAIT = 30;

    final WebDriver driver;
    UserActions acts = new UserActions();

    public BasePage() {
        this.driver = Utils.getWebDriver();;
    }
}
