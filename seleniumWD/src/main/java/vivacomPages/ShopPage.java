package vivacomPages;

public class ShopPage extends BasePage{

    public void chooseMobileDevice(){
        acts.clickElementByXpath("huawei.CheckBox");
        acts.clickElementByXpath("showMoreColors.Btn");
        acts.clickElementByXpath("crushBlue.CheckBox");
        acts.waitForElementClickable("huaweiDevice.Pic",200);
        acts.clickElementByXpath("huaweiDevice.Pic");
    }

    public void navigateToAccessories(){
        acts.waitForElementClickable("devicesStore.Btn", MEDIUM_WAIT);
        acts.fixedWait(2000);
        acts.clickElementByXpath("devicesStore.Btn");

        acts.clickElementByXpath("accessoriesStore.Btn");
    }
    public void filterPrice(){
        acts.waitForElementClickable("price.CheckBox",SHORT_WAIT);
        acts.scrollByVisibleElementJS("price.CheckBox");
        acts.clickElementJS("price.CheckBox");
    }


    public void pickHeadphones(){
        acts.scrollByVisibleElementJS("showMore.Btn");
        acts.clickElementJS("showMore.Btn");

        if(acts.checkIfElementNotPresent("kosmos.Name")){
            acts.refreshPage();
            acts.scrollByVisibleElementJS("showMore.Btn");
            acts.clickElementJS("showMore.Btn");
        }

        acts.waitForElementClickable("kosmos.Name",SHORT_WAIT);
        acts.clickElementByXpath("kosmos.Name");
    }

}
