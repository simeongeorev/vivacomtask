package vivacomPages;

public class PopUpPages extends BasePage {

    public void assertAndCloseOrderPopUp(){
        acts.waitForElementClickable("orderForwardPopUpClose.Btn",MEDIUM_WAIT);
        acts.assertElementPresent("orderForwardPopUp.Text");
        acts.clickElementByXpath("orderForwardPopUpClose.Btn");
    }
}
