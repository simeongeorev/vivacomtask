package vivacomPages;

public class ShoppingCartPage extends BasePage{


    public void returnToShopping () {
        acts.waitForElementClickable("returnToShopping.Btn", MEDIUM_WAIT);
        acts.fixedWait(2000);
        acts.clickElementByXpath("acceptCookies.Btn");
        acts.scrollByVisibleElementJS("returnToShopping.Btn");

        acts.assertElementPresent("returnToShopping.Btn");
        acts.clickElementByXpath("returnToShopping.Btn");
    }

    public void assertShoppingCartPage(){
        acts.waitForElementClickable("returnToShopping.Btn", LONG_WAIT);
        acts.assertElementPresent("returnToShopping.Btn");
    }
    public void checkTheOverallPrice(){
        String priceStr=acts.getElementStringValue("price.Text");
        double sum = Double.parseDouble(priceStr.substring(0, priceStr.length() - 4));
        if(sum>600){
            acts.clickElementJS("headphonesRemove.Btn");
        }
    }
    public void fillCreditScoreCheckBox(){

        acts.waitForElementClickable("creditScore.CheckBox", SHORT_WAIT);
        acts.scrollByVisibleElementJS("creditScore.CheckBox");
        acts.fixedWait(4000);


        acts.clickElementJS("creditScore.CheckBox");

        acts.assertCheckBoxIsChecked("creditScoreAssert.CheckBox");
        acts.assertElementNotPresent("orderForward.Btn");
    }
    public void fillTermsAndCondCheckBox(){
        acts.clickElementByXpath("termsAndCond.CheckBox");

        acts.assertElementPresent("orderForward.Btn");
        acts.clickElementByXpath("orderForward.Btn");
    }
    public void removePhone(){
        acts.waitForElementClickable("huaweiPhoneRemove.Btn", SHORT_WAIT);
        acts.clickElementByXpath("huaweiPhoneRemove.Btn");
    }
    public void assertCartIsEmpty(){
        acts.waitForElementClickable("login.Btn", LONG_WAIT);
        acts.assertElementPresent("emptyCart.Text");
    }

}
