package testCases;

import org.junit.After;
import org.junit.Before;
import testframework.UserActions;

public class BaseTest {


	@Before
	public  void setUp(){
		UserActions.loadBrowser();
	}

	@After
	public void tearDown(){
		UserActions.quitDriver();
	}
}


