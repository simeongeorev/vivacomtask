package testCases;

import org.junit.Test;
import vivacomPages.*;

public class Test_VivacomShoppingCart extends BaseTest {

    HomePage homePage = new HomePage();
    ShopPage shopPage = new ShopPage();
    DevicePage devicePage = new DevicePage();
    ShoppingCartPage cartPage = new ShoppingCartPage();
    PopUpPages popUps = new PopUpPages();

    @Test
    public void shoppingCartTest(){

        homePage.navigateToDevicesStore();

        shopPage.chooseMobileDevice();

        devicePage.addPhoneToCart();

        cartPage.returnToShopping();

        shopPage.navigateToAccessories();
        shopPage.filterPrice();
        shopPage.pickHeadphones();

        devicePage.addHeadphonesToCart();

        cartPage.assertShoppingCartPage();
        cartPage.checkTheOverallPrice();
        cartPage.fillCreditScoreCheckBox();
        cartPage.fillTermsAndCondCheckBox();

        popUps.assertAndCloseOrderPopUp();

        cartPage.removePhone();

        cartPage.assertCartIsEmpty();
    }
}
